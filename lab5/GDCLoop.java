public class GDCLoop{
    
    public static void main(String[] args){
        int small,big,temp;
        if(Integer.parseInt(args[0])>Integer.parseInt(args[1])){
            small=Integer.parseInt(args[1]);
            big=Integer.parseInt(args[0]);
        }  
        else{
            small=Integer.parseInt(args[0]);
            big=Integer.parseInt(args[1]);
        }
        while(small!=0){
            temp=small;
            small = big%small;
            big=temp;
        }
        System.out.println(big);
    }

}