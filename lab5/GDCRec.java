public class GDCRec{
    public static void main(String[] args) {
        int small = Integer.parseInt(args[0]);
        int big = Integer.parseInt(args[1]);
        int temp;
        if(big<small){
            temp=big;
            big=small;
            small=temp;
        }
        int result = GCD(big, small);
        System.out.println(result);

    }
    public static int GCD(int big,int small){
        if(small==0){
            return big;
        }
        else{
            return GCD(small,big%small);
        }
    }
}