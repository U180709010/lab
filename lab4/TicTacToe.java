import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		int count=1;
		int element=1;
		boolean isWon;

		System.out.print("Player 1 enter row number:");
		int row = reader.nextInt();
		System.out.print("Player 1 enter column number:");
		int col = reader.nextInt();
		
		do{
			if(count%2==1){
				board[row - 1][col - 1] = 'X';
				printBoard(board);
				isWon=check(row,col,board);
				if(isWon){
					System.out.println("X won!");
				}
			}else{
				board[row - 1][col - 1] = 'O';
				printBoard(board);
				isWon=check(row,col,board);
				if(isWon){
					System.out.println("O won!");
				}
			}
			
			
			System.out.print("Player " + (count+1) +  " enter row number:");
			row = reader.nextInt();
			System.out.print("Player " + (count+1) +  " enter column number:");
			col = reader.nextInt();
			while(board[row-1][col-1]!=' '||){
				System.out.println("Please enter a valid location");
				System.out.print("Player " + (count+1) +  " enter row number:");
				row = reader.nextInt();
				System.out.print("Player " + (count+1) +  " enter column number:");
				col = reader.nextInt();
			}
			count++;
			element++;	
		}while(board[row-1][col-1]==' ' && element!=9);
		

		

		reader.close();
	}

	public static boolean check(int row,int column,char[][] board){
		char ch = board[row-1][column-1];
		boolean winFLag = true;
		for(int i=0;i<3;i++){
			if(board[row-1][i]!=ch){
				winFLag=false;
				break;
			}	
		}
		if(winFLag){
			return true;
		}
		for(int i=0;i<3;i++){
			if(board[i][column-1]!=ch){
				winFLag=false;
				break;
			}	
		}
		
		if(winFLag){
			return true;
		}
		if((row-1==0&&column-1==0)||(row-1==2&&column-1==2)){
			for(int i=0;i<3;i++){
				if(board[i][i]!=ch){
					winFLag=false;
					break;
				}
			}
		}
		if(winFLag){
			return true;
		}
		if((row-1==0&&column-1==2)||(row-1==2&&column-1==0)){
			for(int i=0;i<3;i++){
				if(board[i][2-i]!=ch){
					winFLag=false;
					break;
				}
			}
		}
		return winFLag;
	}

	

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
