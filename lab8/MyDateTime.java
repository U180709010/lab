

public class MyDateTime{
    public MyDate date;
    public MyTime time;
    public MyDateTime(MyDate date,MyTime time){
        this.time= time;
        this.date = date;
    }
    public void incrementDay(){
        incrementDay(1);
    }
    public void incrementYear(int year){
        date.incrementYear(year);
    }
    public void decrementDay(){
        date.decrementDay();
    }
    public void decrementYear(){
        date.decrementYear();
    }
    public void decrementMonth(){
        date.decrementMonth(1);
    }
    public void decrementMonth(int month){
         date.decrementMonth(month);
    }
    public void incrementDay(int day){
        date.incrementDay(day);
    }
    public void decrementDay(int day){
        date.decrementDay(day);
    }
    public void incrementMonth(int month){
        date.incrementMonth(month);
    }
    public void decrementYear(int year){
        date.decrementYear(year);
    }
    public void incrementMonth(){
        date.incrementMonth();
    }
    public void incrementYear(){
        date.incrementYear();
    }
    public void incrementHour(){
        int dayDiff = time.incrementHour(1);
        date.incrementDay(dayDiff);
    }
    public void incrementHour(int hour){
        int dayDiff = time.incrementHour(hour);
        date.incrementDay(dayDiff);
    }
    public void decrementHour(int hour){
        int dayDiff = time.decrementHour(hour);
        date.decrementDay(dayDiff);
    }
    public void incrementMinute(int minute){
        int hourDiff = time.incrementMinute(minute);
        int dayDiff = time.incrementHour(hourDiff);
        date.incrementDay(dayDiff);

    }
    public void decrementMinute(int minute){
        int hourDiff = time.decrementMinute(minute);
        int dayDiff = time.decrementHour(hourDiff);
        date.decrementDay(dayDiff);
    }
    public String toString(){
        return date.toString() + " " + time.toString();
    }
    public boolean isBefore(MyDateTime anotherDateTime){
        if(this.date.isBefore(anotherDateTime.date)){
            return true;
        }else if(date.isAfter(anotherDateTime.date)){
            return false;
        }else{
            if(time.isBefore(anotherDateTime.time)){
                return true;
            }else{
                return false;
            }
        }
        
    }
    public boolean isAfter(MyDateTime anotherDateTime){
        if(this.date.isAfter(anotherDateTime.date)){
            return true;
        }else if(date.isBefore(anotherDateTime.date)){
            return false;
        }else{
            if(time.isAfter(anotherDateTime.time)){
                return true;
            }else{
                return false;
            }
        }
    }
    public String dayTimeDifference(MyDateTime anotherDateTime){
        int dayDiff = date.dayDifference(anotherDateTime.date);
        int minuteDiff = time.minuteDifference(anotherDateTime.time);
        if(minuteDiff<0){
            minuteDiff = minuteDiff + 1440;
            String response = (dayDiff-1) + " day(s) " + (minuteDiff / 60) + " hour(s) " + (minuteDiff % 60) + " minute(s) ";
            return response;
        }else{
            String response = dayDiff + " day(s) " + (minuteDiff / 60) + " hour(s) " + (minuteDiff % 60) + " minute(s) ";
            return response;
        }
    }
}