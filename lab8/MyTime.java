

public class MyTime{
    public int hour;
    public int minute;
    public MyTime(int hour,int minute){
        this.hour = hour;
        this.minute=minute;
    }
    public int incrementHour(int hour){
        int totalHour = this.hour + hour;
        int dayDifference = totalHour / 24;
        this.hour = (totalHour) % 24;
        
        return dayDifference;
    }
    public int decrementHour(int hour){
        int totalHour = this.hour - hour;
        int dayDifference = 0;
        if(totalHour<0){
            this.hour = (+totalHour%24) + 24;
        }else{
            this.hour = totalHour;
        }
        while(totalHour<0){
            dayDifference++;
            totalHour = totalHour + 24;
        }
        return dayDifference;
    }
    public int incrementMinute(int minute){
        int totalMinute = this.minute + minute;
        int hourDifference = totalMinute / 60;
        this.minute = (totalMinute%3600) % 60;
        return hourDifference;
    }
    public int decrementMinute(int minute){
        int totalMinute = this.minute - minute;
        int hourDifference = 0;
        if(totalMinute < 0){
            this.minute = (((totalMinute)%3600) % 60) + 60;
        }else{
            this.minute = totalMinute;
        }
        while(totalMinute<0){
            hourDifference++;
            totalMinute = totalMinute + 60;
        }
        return hourDifference;
    }
    public String toString(){
        return (hour < 10 ? "0" : "") + hour + ":" + (minute < 10 ? "0" : "") + minute;
    }
    public boolean isBefore(MyTime anotherTime){
        if(hour < anotherTime.hour){
            return true;
        }else if(hour > anotherTime.hour){
            return false;
        }else{
            if(minute < anotherTime.minute){
                return true;
            }else{
                return false;
            }
        }
    }
    public boolean isAfter(MyTime anotherTime){
        if(hour > anotherTime.hour){
            return true;
        }else if(hour < anotherTime.hour){
            return false;
        }else{
            if(minute > anotherTime.minute){
                return true;
            }else{
                return false;
            }
        }
    }
    public int timeDifference(MyTime anotherTime){
        int ourMinutes = (hour*60) + minute;
        int anotherMinutes = (anotherTime.hour*60) + anotherTime.minute;
        if(ourMinutes < anotherMinutes){
            return (anotherMinutes - ourMinutes) % 60;
        }else{
            return ( ourMinutes - anotherMinutes) % 60;
        }
    }
    public int minuteDifference(MyTime anotherTime){
        int ourMinutes = (this.hour * 60) + this.minute;
        int anotherMinutes = (anotherTime.hour * 60) + anotherTime.minute;
        return anotherMinutes - ourMinutes;
    }
    
}