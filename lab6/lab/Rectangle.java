package lab;

public class Rectangle {
	private int sideA;
	private int sideB;
	private Point topLeft;
	
	public Rectangle(int sideA,int sideB,Point topLeft) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.topLeft = topLeft;
	}
	
	public int area() {
		return sideA*sideB;
	}
	
	public int perimeter() {
		return (2*sideA)+(2*sideB);
	}
	
	public Point[] corners(){
		Point[] points = new Point[4];
		points[0] = topLeft;
		points[1] = new Point(sideA+topLeft.xCoord,topLeft.yCoord);
		points[2] = new Point(topLeft.xCoord,topLeft.yCoord-sideB);
		points[3] = new Point(sideA+topLeft.xCoord,topLeft.yCoord-sideB);
		return points;
	}
	
	
}
