

public class FindPrimeNumbers{
    public static void main(String[] args){
        FindPrimes(args[0]);
    }
    public static void FindPrimes(int num){
        for(int i=2;i<num;i++){
            if(isPrime(i)){
                System.out.print(i + " ");
            }
        }
    }
    public static boolean isPrime(int num){
        if(num==2)
            return true;
        for(int i=2;i<=(num/2);i++){
            if(num%i==0)
                return false;
        }
        return true;
    }
    
}