package shapes2d;

import shapes3d.*;

public class TestShapes2d {

	public static void main(String[] args) {
		Circle c = new Circle(3);
		System.out.println(c.toString());
		Square s = new Square(2);
		System.out.println(s.getSide());
		Cylinder ce = new Cylinder(2,5);
		System.out.println(ce.toString());
		Cube cu = new Cube(4);
		System.out.println(cu.volume());
		
	}

}
