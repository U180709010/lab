package stack;

public class StackItem {
	private Object item;
	private StackItem next;
	
	public StackItem(Object item) {
		this.item = item;
	}
	
	public Object getObj() {
		return this.item;
	}
	
	public StackItem getNext() {
		return this.next;
	}
	
	public void setNext(StackItem next) {
		this.next = next;
	}
	
}
