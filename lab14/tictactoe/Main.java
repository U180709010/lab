package tictactoe;


import java.util.InputMismatchException;
import java.util.Scanner;

public class Main{

	public static void main(String[] args)  {
		Scanner reader = new Scanner(System.in);

		Board board = new Board();
		

		System.out.println(board);
		myLoop: while (!board.isEnded()) {
			
			int player = board.getCurrentPlayer();
			int row;
			try {
				System.out.print("Player "+ player + " enter row number:");
				row = reader.nextInt();
				
			}catch(InputMismatchException e){
				System.out.println("Row is not integer");
				reader.nextLine();
				continue myLoop;
			}
			int col;
			try {
				System.out.print("Player "+ player + " enter column number:");
				col = reader.nextInt();
			}catch(InputMismatchException a) {
				System.out.println("Column is not integer");
				reader.nextLine();
				continue myLoop;
			}
			try {
				board.move(row, col);
			} catch (InvalidMoveException c) {
				System.out.println("Invalid Move");
				continue myLoop;
			}
			System.out.println(board);
			
		}
		
		
		reader.close();
	}



}
